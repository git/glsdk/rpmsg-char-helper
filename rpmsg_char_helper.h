/*
 * Texas Instruments Rpmsg Char Helper library 
 *
 * Copyright (c) 2018 Texas Instruments, Inc.
 * Author: Subhajit Paul <subhajit_paul@ti.com>
 * Description: Rpmsg Char Helper interface header file
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

#ifndef __RPMSG_CHAR_HELPER_H__
#define __RPMSG_CHAR_HELPER_H__

struct rproc_device;
struct rproc_char_device;
struct rproc_char_endpt;

typedef struct rproc_device rproc_device_t;
typedef struct rproc_char_device rproc_char_device_t;
typedef struct rproc_char_endpt rproc_char_endpt_t;


char **rproc_get_supported_names(void);

rproc_device_t *rproc_device_find_for_name(const char *name);

rproc_char_device_t *rproc_device_find_chrdev_by_name(rproc_device_t *dev, char *desc_name);
rproc_char_device_t *rproc_device_find_chrdev_by_remote_port(rproc_device_t *dev, int remote_port);
char *rproc_char_device_get_dev_name(rproc_char_device_t *cdev);

rproc_char_endpt_t *rproc_char_device_create_endpt(rproc_char_device_t *cdev, char *name, int local_port);
rproc_char_endpt_t *rproc_char_device_find_endpt_by_name(rproc_char_device_t *cdev, char *name);
rproc_char_endpt_t *rproc_char_device_find_endpt_by_local_port(rproc_char_device_t *cdev, int local_port);
char *rproc_char_endpt_get_dev_name(rproc_char_endpt_t *ept);
int rproc_char_endpt_get_local_port(rproc_char_endpt_t *ept);

int rproc_char_endpt_destroy(rproc_char_endpt_t *ept);

#endif

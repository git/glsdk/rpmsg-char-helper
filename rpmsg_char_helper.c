/*
 * Texas Instruments Rpmsg Char Helper library
 *
 * Copyright (c) 2018 Texas Instruments, Inc.
 * Author: Subhajit Paul <subhajit_paul@ti.com>
 * Description: Rpmsg Char Helper implementation file
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */

#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <stdarg.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include <stdint.h>

#define RPMSG_ADDR_ANY		0xFFFFFFFF

struct rpmsg_endpoint_info {
	char name[32];
	uint32_t src;
	uint32_t dst;
};

#define RPMSG_CREATE_EPT_IOCTL	_IOW(0xb5, 0x1, struct rpmsg_endpoint_info)
#define RPMSG_DESTROY_EPT_IOCTL	_IO(0xb5, 0x2)

#include "rpmsg_char_helper.h"

#define printerr printf

struct rproc_device {
	void *priv;
	unsigned int remoteproc_id;
	unsigned int virtio_id;
};

struct rproc_char_device {
	struct rproc_device *dev;
	unsigned int ctrl_id;
	int dst;
	int src;
};

struct rproc_char_endpt {
	struct rproc_char_device *cdev;
	unsigned int rpmsg_id;
};

struct rproc_map {
	char *rproc_user_name;
	char *sysfs_path;
};

#if defined (SOC_J7)

struct rproc_map maps[] = {
	{
		.rproc_user_name = "r5f-mcu-core-0",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:interconnect@28380000/interconnect@100000:interconnect@28380000:r5fss@41000000/41000000.r5f",
	},
	{
		.rproc_user_name = "r5f-mcu-core-1",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:interconnect@28380000/interconnect@100000:interconnect@28380000:r5fss@41000000/41400000.r5f",
	},
	{
		.rproc_user_name = "r5f-main-0-core-0",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:r5fss@5c00000/5c00000.r5f",
	},
	{
		.rproc_user_name = "r5f-main-0-core-1",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:r5fss@5c00000/5d00000.r5f",
	},
	{
		.rproc_user_name = "r5f-main-1-core-0",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:r5fss@5e00000/5e00000.r5f",
	},
	{
		.rproc_user_name = "r5f-main-1-core-1",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/interconnect@100000:r5fss@5e00000/5f00000.r5f",
	},
	{
		.rproc_user_name = "c66-0",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/4d80800000.dsp",
	},
	{
		.rproc_user_name = "c66-1",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/4d81800000.dsp",
	},
	{
		.rproc_user_name = "c7x",
		.sysfs_path = "/sys/devices/platform/interconnect@100000/64800000.dsp",
	},
};
#else
struct rproc_map maps[0];
#endif

#define NUM_RPROCS ((sizeof(maps))/(sizeof(struct rproc_map)))

char **rproc_get_supported_names()
{
	int i;
	bool fail = false;
	char **names = calloc(NUM_RPROCS + 1, sizeof(char *));
	if(!names)
		return NULL;

	for(i = 0; i < NUM_RPROCS; i++) {
		names[i] = strdup(maps[i].rproc_user_name);
		if(!names[i]) {
			fail = true;
			break;
		}
	}

	if(fail) {
		for(i = 0; fail && i < NUM_RPROCS; i++)
			if(names[i])
				free(names[i]);
		free(names);
		names = NULL;
	}

	return names;
}

static char *find_child_dir_by_name(DIR *parent, char *child_name)
{
	struct dirent *iter;

	if(!parent)
		return NULL;

	while((iter = readdir(parent)))
		if(!strcmp(iter->d_name, child_name) && iter->d_type == DT_DIR)
			break;

	if(iter)
		return strdup(iter->d_name);

	return NULL;
}

static char *find_child_dir_by_uint_suffix(DIR *parent, const char *child_name_pattern, unsigned int *suffix)
{
	struct dirent *iter;

	if(!parent)
		return NULL;

	while((iter = readdir(parent)))
		if(iter->d_type == DT_DIR && sscanf(iter->d_name, child_name_pattern, suffix))
			break;

	if(iter)
		return strdup(iter->d_name);

	return NULL;
}

static char *str_join(const char *fmt, ...)
{
	int size = 0;
	char *p = NULL;
	va_list ap;

	/* Determine required size */

	va_start(ap, fmt);
	size = vsnprintf(p, size, fmt, ap);
	va_end(ap);

	if (size <= 0)
		return NULL;

	size++; /* For '\0' */
	p = malloc(size);
	if (p == NULL)
		return NULL;

	va_start(ap, fmt);
	size = vsnprintf(p, size, fmt, ap);
	if (size <= 0) {
		free(p);
		return NULL;
	}
	va_end(ap);

	return p;
}

rproc_device_t *rproc_device_find_for_name(const char *name)
{
	int i;
	struct rproc_map *r = NULL;
	DIR *sysfs_dir, *remoteproc_dir = NULL, *remoteproc_node_dir = NULL, *virtio_node_dir = NULL;
	struct rproc_device *dev = NULL;
	char *child;
	char *remoteproc_path = NULL, *remoteproc_node_path = NULL, *virtio_node_path = NULL;
	unsigned int remoteproc_id;
	unsigned int virtio_id;

	if(!name) {
		printerr("%s: name = NULL not allowed\n", __func__);
		return NULL;
	}

	for(i = 0; i < NUM_RPROCS; i++) {
		if(strcmp(maps[i].rproc_user_name, name) == 0) {
			r = &maps[i];
			break;
		}
	}
	if(!r) {
		printerr("%s: unsupported rproc %s\n", __func__, name);
		return NULL;
	}

	/* open the corresponding sysfs path for iterating */
	sysfs_dir = opendir(r->sysfs_path);
	if(!sysfs_dir) {
		printerr("%s: could not open sysfs directory for %s [error: %s]\n", __func__, name, strerror(errno));
		return NULL;
	}

	/* find and open a child directory named <sysfs_path>/remoteproc */
	child = find_child_dir_by_name(sysfs_dir, "remoteproc");
	if(child) {
		remoteproc_path = str_join("%s/%s", r->sysfs_path, child);
		free(child);
	}
	if(remoteproc_path)
		remoteproc_dir = opendir(remoteproc_path);

	if(!child || !remoteproc_path || !remoteproc_dir) {
		printerr("%s: could not open remoteproc directory for %s\n", __func__, name);
		if(remoteproc_path)
			free(remoteproc_path);
		goto remoteproc_nodir;
	}

	/* find and open a child directory of pattern <sysfs_path>/remoteproc/remoteproc%d */
	child = find_child_dir_by_uint_suffix(remoteproc_dir, "remoteproc%u", &remoteproc_id);
	if(child) {
		remoteproc_node_path = str_join("%s/%s/remoteproc%u#vdev0buffer/", remoteproc_path, child, remoteproc_id);
		free(child);
	}
	if(remoteproc_node_path)
		remoteproc_node_dir = opendir(remoteproc_node_path);

	if(!child || !remoteproc_node_path || !remoteproc_node_dir) {
		printerr("%s: could not open remoteproc node directory for %s\n", __func__, name);
		if(remoteproc_node_path)
			free(remoteproc_node_path);
		goto remoteproc_node_nodir;
	}

	/* find and open a child directory of pattern <sysfs_path>/remoteproc/remoteproc%d/virtio%d */
	child = find_child_dir_by_uint_suffix(remoteproc_node_dir, "virtio%u", &virtio_id);
	if(child) {
		virtio_node_path = str_join("%s/%s", remoteproc_node_path, child);
		free(child);
	}
	if(virtio_node_path)
		virtio_node_dir = opendir(virtio_node_path);

	if(!child || !virtio_node_path || !virtio_node_dir) {
		printerr("%s: could not open virtio node directory for %s\n", __func__, name);
		if(virtio_node_path)
			free(virtio_node_path);
		goto virtio_node_nodir;
	}

	dev = calloc(sizeof(*dev), 1);
	if(!dev) {
		printerr("%s: could not allocate memory for %s\n", __func__, name);
		goto calloc_fail;
	}

	dev->priv = r;
	dev->remoteproc_id = remoteproc_id;
	dev->virtio_id = virtio_id;

calloc_fail:
	free(virtio_node_path);
	closedir(virtio_node_dir);
virtio_node_nodir:
	free(remoteproc_node_path);
	closedir(remoteproc_node_dir);
remoteproc_node_nodir:
	free(remoteproc_path);
	closedir(remoteproc_dir);
remoteproc_nodir:
	closedir(sysfs_dir);
	return dev;
}

rproc_char_device_t *rproc_device_find_chrdev_by_name(rproc_device_t *dev, char *desc_name)
{
	printerr("%s: unsupported feature\n", __func__);
	return NULL;
}

rproc_char_device_t *rproc_device_find_chrdev_by_remote_port(rproc_device_t *dev, int remote_port)
{
	struct rproc_char_device *chrdev = NULL;
	struct rproc_map *r;
	char *virtio_node_path, *rpmsg_path = NULL, *chrdev_node_path = NULL;
	DIR *virtio_node_dir, *rpmsg_dir = NULL, *chrdev_node_dir = NULL;
	struct dirent *iter;
	int n;
	int dst, src;
	unsigned int virtio_id, ctrl_id;
	char *child;

	if(!dev) {
		printerr("%s: dev = NULL not allowed\n", __func__);
		return NULL;
	}

	r = dev->priv;
	if(!r || (r - &maps[0]) >= NUM_RPROCS) {
		printerr("%s: malformed dev -> bad priv\n", __func__);
		return NULL;
	}

	virtio_node_path = str_join("%s/remoteproc/remoteproc%u/remoteproc%u#vdev0buffer/virtio%u", r->sysfs_path, dev->remoteproc_id, dev->remoteproc_id, dev->virtio_id);
	if(virtio_node_path)
		virtio_node_dir = opendir(virtio_node_path);
	if(!virtio_node_path || !virtio_node_dir) {
		printerr("%s: could not open virtio node directory [%s] for %s\n", __func__, virtio_node_path, r->rproc_user_name);
		if(virtio_node_path)
			free(virtio_node_path);
		return NULL;
	}

	/* find a directory that reads virtio%d.rpmsg_chrdev.%d.%d */
	while((iter = readdir(virtio_node_dir))) {
		if(iter->d_type == DT_DIR) {
			n = sscanf(iter->d_name, "virtio%u.rpmsg_chrdev.%d.%d", &virtio_id, &src, &dst);
			if(n == 3 && virtio_id == dev->virtio_id && dst == remote_port)
				break;
		}
	}
	if(!iter) {
		printerr("%s: could not find a matching rpmsg_chrdev node\n", __func__);
		goto chrdev_nodir;
	}

	chrdev_node_path = str_join("%s/%s", virtio_node_path, iter->d_name);
	if(chrdev_node_path)
		chrdev_node_dir = opendir(chrdev_node_path);
	if(!chrdev_node_path || !chrdev_node_dir) {
		printerr("%s: could not open rpmsg_chrdev node directory for %s\n", __func__, r->rproc_user_name);
		if(chrdev_node_path)
			free(chrdev_node_path);
		goto chrdev_nodir;
	}

	/* find and open a child directory named rpmsg*/
	child = find_child_dir_by_name(chrdev_node_dir, "rpmsg");
	if(child) {
		rpmsg_path = str_join("%s/%s", chrdev_node_path, child);
		free(child);
	}
	if(rpmsg_path)
		rpmsg_dir = opendir(rpmsg_path);

	if(!child || !rpmsg_path || !rpmsg_dir) {
		printerr("%s: could not open rpmsg directory for %s\n", __func__, r->rproc_user_name);
		if(rpmsg_path)
			free(rpmsg_path);
		goto rpmsg_nodir;
	}

	/* find and open a child directory of pattern rpmsg/rpmsg_ctrl%d */
	child = find_child_dir_by_uint_suffix(rpmsg_dir, "rpmsg_ctrl%u", &ctrl_id);
	if(!child) {
		printerr("%s: could not open rpmsg_ctrl node directory for %s\n", __func__, r->rproc_user_name);
		goto ctrl_nodir;
	}
	free(child);

	chrdev = calloc(sizeof(*chrdev), 1);
	if(!chrdev) {
		printerr("%s: could not allocate memory for %s\n", __func__, r->rproc_user_name);
		goto ctrl_nodir;
	}

	chrdev->dev = dev;
	chrdev->ctrl_id = ctrl_id;
	chrdev->src = src;
	chrdev->dst = dst;

ctrl_nodir:
	free(rpmsg_path);
	closedir(rpmsg_dir);
rpmsg_nodir:
	free(chrdev_node_path);
	closedir(chrdev_node_dir);
chrdev_nodir:
	free(virtio_node_path);
	closedir(virtio_node_dir);
	return chrdev;
}

char *rproc_char_device_get_dev_name(rproc_char_device_t *cdev)
{
	if(!cdev) {
		printerr("%s: cdev = NULL not allowed\n", __func__);
		return NULL;
	}

	return str_join("/dev/rpmsg_ctrl%u", cdev->ctrl_id);
}

rproc_char_endpt_t *rproc_char_device_create_endpt(rproc_char_device_t *cdev, char *name, int local_port)
{
	int fd, ret;
	char *path;
	struct rproc_device *dev;
	struct rproc_map *r;
	struct rpmsg_endpoint_info ept;

	if(!cdev) {
		printerr("%s: cdev = NULL not allowed\n", __func__);
		return NULL;
	}

	dev = cdev->dev;
	if(!dev) {
		printerr("%s: malformed cdev -> dev = NULL\n", __func__);
		return NULL;
	}

	r = dev->priv;
	if(!r || (r - &maps[0]) >= NUM_RPROCS) {
		printerr("%s: malformed cdev -> bad dev->priv \n", __func__);
		return NULL;
	}

	path = str_join("/dev/rpmsg_ctrl%u", cdev->ctrl_id);
	if(!path) {
		printerr("%s: could not allocate memory\n", __func__);
		return NULL;
	}

	fd = open(path, O_RDWR);
	if(fd < 0) {
		printerr("%s: could not open rpmsg_ctrl dev node for %s\n", __func__, r->rproc_user_name);
		goto free_path;
	}
	free(path);

	memset(&ept, 0, sizeof(ept));
	ept.src = local_port;
	ept.dst = cdev->dst;
	sprintf(ept.name, "%s", name);

	ret = ioctl(fd, RPMSG_CREATE_EPT_IOCTL, &ept);
	if(ret) {
		printerr("%s: ctrl_fd ioctl: %s\n", __func__, strerror(errno));
		goto close_fd;
	}

	return rproc_char_device_find_endpt_by_local_port(cdev, ept.src);

close_fd:
	close(fd);
free_path:
	free(path);
	return NULL;
}

rproc_char_endpt_t *rproc_char_device_find_endpt_by_local_port(rproc_char_device_t *cdev, int local_port)
{
	struct rproc_device *dev;
	struct rproc_map *r;
	struct rproc_char_endpt *ept = NULL;
	char *ctrl_path = NULL;
	DIR *ctrl_dir = NULL;
	struct dirent *iter;
	unsigned int rpmsg_id;

	if(!cdev) {
		printerr("%s: cdev = NULL not allowed\n", __func__);
		return NULL;
	}

	if(local_port == RPMSG_ADDR_ANY) {
		printerr("%s: local_port = RPMSG_ADDR_ANY not allowed\n", __func__);
		return NULL;
	}

	dev = cdev->dev;
	if(!dev) {
		printerr("%s: malformed cdev -> dev = NULL\n", __func__);
		return NULL;
	}

	r = dev->priv;
	if(!r || (r - &maps[0]) >= NUM_RPROCS) {
		printerr("%s: malformed cdev -> bad dev->priv \n", __func__);
		return NULL;
	}

	ctrl_path = str_join("%s/remoteproc/remoteproc%u/remoteproc%u#vdev0buffer/virtio%u/virtio%u.rpmsg_chrdev.%d.%d/rpmsg/rpmsg_ctrl%u",
			r->sysfs_path, dev->remoteproc_id, dev->remoteproc_id, dev->virtio_id, dev->virtio_id,
			cdev->src, cdev->dst, cdev->ctrl_id);
	if(ctrl_path)
		ctrl_dir = opendir(ctrl_path);
	if(!ctrl_path || !ctrl_dir) {
		printerr("%s: could not open rpmsg_ctrl directory for %s\n", __func__, r->rproc_user_name);
		if(ctrl_path)
			free(ctrl_path);
		return NULL;
	}

	while((iter = readdir(ctrl_dir)))
	{
		if(iter->d_type == DT_DIR && sscanf(iter->d_name, "rpmsg%u", &rpmsg_id)) {
			char *path;
			int fd;
			char buf[64];
			int ret;

			path = str_join("%s/%s/src", ctrl_path, iter->d_name);
			if(!path)
				continue;

			fd = open(path, O_RDONLY);
			free(path);

			if(fd < 0)
				continue;

			ret = read(fd, buf, 64);
			close(fd);

			if(ret < 0)
				continue;

			buf[ret] = '\0';
			if(ret > 0 && local_port == atoi(buf))
				break;
		}
	}

	if(!iter) {
		printerr("%s: could not find rpmsg directory for %s->%d\n", __func__, r->rproc_user_name, local_port);
		goto out;
	}

	ept = calloc(sizeof(*ept), 1);
	if(!ept) {
		printerr("%s: could not allocate memory\n", __func__);
		goto out;
	}

	ept->cdev = cdev;
	ept->rpmsg_id = rpmsg_id;

out:
	free(ctrl_path);
	closedir(ctrl_dir);
	return ept;
}

rproc_char_endpt_t *rproc_char_device_find_endpt_by_name(rproc_char_device_t *cdev, char *name)
{
	struct rproc_device *dev;
	struct rproc_map *r;
	struct rproc_char_endpt *ept = NULL;
	char *ctrl_path = NULL;
	DIR *ctrl_dir = NULL;
	struct dirent *iter;
	unsigned int rpmsg_id;

	if(!cdev) {
		printerr("%s: cdev = NULL not allowed\n", __func__);
		return NULL;
	}

	dev = cdev->dev;
	if(!dev) {
		printerr("%s: malformed cdev -> dev = NULL\n", __func__);
		return NULL;
	}

	r = dev->priv;
	if(!r || (r - &maps[0]) >= NUM_RPROCS) {
		printerr("%s: malformed cdev -> bad dev->priv \n", __func__);
		return NULL;
	}

	ctrl_path = str_join("%s/remoteproc/remoteproc%u/remoteproc%u#vdev0buffer/virtio%u/virtio%u.rpmsg_chrdev.%d.%d/rpmsg/rpmsg_ctrl%u",
			r->sysfs_path, dev->remoteproc_id, dev->remoteproc_id, dev->virtio_id, dev->virtio_id,
			cdev->src, cdev->dst, cdev->ctrl_id);
	if(ctrl_path)
		ctrl_dir = opendir(ctrl_path);
	if(!ctrl_path || !ctrl_dir) {
		printerr("%s: could not open rpmsg_ctrl directory for %s\n", __func__, r->rproc_user_name);
		if(ctrl_path)
			free(ctrl_path);
		return NULL;
	}

	while((iter = readdir(ctrl_dir)))
	{
		if(iter->d_type == DT_DIR && sscanf(iter->d_name, "rpmsg%u", &rpmsg_id)) {
			char *path;
			int fd;
			char buf[32];
			int ret;

			path = str_join("%s/%s/name", ctrl_path, iter->d_name);
			if(!path)
				continue;

			fd = open(path, O_RDONLY);
			free(path);

			if(fd < 0)
				continue;

			ret = read(fd, buf, 32);
			close(fd);

			if(ret < 0)
				continue;

			buf[ret - 1] = '\0';
			if(ret > 0 && strcmp(buf, name) == 0)
				break;
		}
	}

	if(!iter) {
		printerr("%s: could not find rpmsg directory for %s->%s\n", __func__, r->rproc_user_name, name);
		goto out;
	}

	ept = calloc(sizeof(*ept), 1);
	if(!ept) {
		printerr("%s: could not allocate memory\n", __func__);
		goto out;
	}

	ept->cdev = cdev;
	ept->rpmsg_id = rpmsg_id;

out:
	free(ctrl_path);
	closedir(ctrl_dir);
	return ept;

	return ept;
}

int rproc_char_endpt_get_local_port(rproc_char_endpt_t *ept)
{
	struct rproc_device *dev;
	struct rproc_map *r;
	struct rproc_char_device *cdev;
	char *ept_src_path;
	int fd;
	char buf[32];
	int ret;
	uint32_t src;

	if(!ept) {
		printerr("%s: ept = NULL not allowed\n", __func__);
		return -1;
	}

	cdev = ept->cdev;
	if(!cdev) {
		printerr("%s: malformed ept -> cdev = NULL\n", __func__);
		return -1;
	}

	dev = cdev->dev;
	if(!dev) {
		printerr("%s: malformed ept -> dev = NULL\n", __func__);
		return -1;
	}

	r = dev->priv;
	if(!r || (r - &maps[0]) >= NUM_RPROCS) {
		printerr("%s: malformed ept -> bad dev->priv \n", __func__);
		return -1;
	}

	ept_src_path = str_join("%s/remoteproc/remoteproc%u/remoteproc%u#vdev0buffer/virtio%u/virtio%u.rpmsg_chrdev.%d.%d/rpmsg/rpmsg_ctrl%u/rpmsg%u/src",
			r->sysfs_path, dev->remoteproc_id, dev->remoteproc_id, dev->virtio_id, dev->virtio_id,
			cdev->src, cdev->dst, cdev->ctrl_id, ept->rpmsg_id);
	if(ept_src_path)
		fd = open(ept_src_path, O_RDONLY);
	if(!ept_src_path || fd < 0) {
		printerr("%s: could not open endpoint src for %u\n", __func__, ept->rpmsg_id);
		if(ept_src_path)
			free(ept_src_path);
		return -1;
	}
	free(ept_src_path);

	ret = read(fd, buf, 32);
	close(fd);

	if(ret <= 0) {
		printerr("%s: could not read src file\n", __func__);
		return -1;
	}

	buf[ret - 1] = '\0';

	ret = sscanf(buf, "%u", &src);
	if(ret != 1) {
		printerr("%s: could not read src file data\n", __func__);
		return -1;
	}

	return src;
}

char *rproc_char_endpt_get_dev_name(rproc_char_endpt_t *ept)
{
	if(!ept) {
		printerr("%s: ept = NULL not allowed\n", __func__);
		return NULL;
	}

	return str_join("/dev/rpmsg%u", ept->rpmsg_id);
}


int rproc_char_endpt_destroy(rproc_char_endpt_t *ept)
{
	int fd, ret;
	char *path;


	if(!ept) {
		printerr("%s: ept = NULL not allowed\n", __func__);
		return -EINVAL;
	}

	path = str_join("/dev/rpmsg%u", ept->rpmsg_id);
	if(!path) {
		printerr("%s: could not allocate memory\n", __func__);
		return -ENOMEM;
	}

	fd = open(path, O_RDWR);
	if(fd < 0) {
		ret = -errno;
		printerr("%s: could not open file %s [%s]\n", __func__, path, strerror(errno));
		goto free_path;
	}

	ret = ioctl(fd, RPMSG_DESTROY_EPT_IOCTL, NULL);
	if(ret)
		printerr("%s: could not destroy endpt\n", __func__);

       close(fd);
free_path:
       free(path);
       return ret;
}
